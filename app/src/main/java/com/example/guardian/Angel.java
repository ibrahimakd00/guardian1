package com.example.guardian;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class Angel extends AppCompatActivity {
    private EditText nomInput;
    private EditText numInput;
    private EditText mailInput;
    private EditText adresseInput;
    private Button updateBtn;
    private Button deleteBtn;
    private Button retourBtn;
    private DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_angel);

        Intent intent = getIntent();

        nomInput = findViewById(R.id.activity_ange_nom_input);
        numInput = findViewById(R.id.activity_ange_num_input);
        mailInput = findViewById(R.id.activity_ange_mail_input);
        adresseInput = findViewById(R.id.activity_ange_adresse_input);
        deleteBtn = findViewById(R.id.activity_delete_btn);
        updateBtn = findViewById(R.id.activity_update_btn);
        retourBtn = findViewById(R.id.retour_angel);
        db = new DBHelper(this);

        String name = intent.getStringExtra("name");
        Cursor res = db.getAngels(name);
        while (res.moveToNext()){
            nomInput.setText(name);
            numInput.setText(res.getString(0));
            mailInput.setText(res.getString(2));
            adresseInput.setText(res.getString(3));
        }

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nom = nomInput.getText().toString();
                String num = numInput.getText().toString();
                String mail = mailInput.getText().toString();
                String adresse = adresseInput.getText().toString();

                boolean checkUpdate = db.updateAngel(nom,num,mail,adresse);
                if (checkUpdate==true){
                    Toast.makeText(Angel.this, "Ange modifié!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Angel.this, ListeAngel.class);
                    startActivity(intent);
                }
                else
                    Toast.makeText(Angel.this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Angel.this);
                builder.setTitle("Cet ange va être supprimé!");
                builder.setMessage("Voulez-vous vraiment supprimé cet ange?");
                builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String num = numInput.getText().toString();

                        boolean checkDelete = db.deleteAngel(num);
                        if (checkDelete==true){
                            Toast.makeText(Angel.this, "Ange supprimé!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Angel.this, ListeAngel.class);
                            startActivity(intent);
                        }
                        else
                            Toast.makeText(Angel.this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(Angel.this, "Opération annulée", Toast.LENGTH_SHORT).show();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        retourBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View view){
                Intent intent = new Intent(Angel.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}