package com.example.guardian;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(Context context) {
        super(context, "Guardian.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create Table Angels(number TEXT primary key, name TEXT, mail TEXT,adress TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop Table if exists Angels");
    }

    public boolean addAngel(String name, String number, String mail, String adress) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("number", number);
        contentValues.put("mail", mail);
        contentValues.put("adress", adress);
        long result = db.insert("Angels", null, contentValues);
        if (result == 1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean updateAngel(String name, String number, String mail, String adress) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("mail", mail);
        contentValues.put("adress", adress);
        Cursor cursor = db.rawQuery("Select * from Angels where number = ?", new String[]{number});
        if (cursor.getCount() > 0) {
            long result = db.update("Angels", contentValues, "number=?", new String[]{number});
            if (result == -1) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public boolean deleteAngel(String number) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Angels where number = ?", new String[]{number});
        if (cursor.getCount() > 0) {
            long result = db.delete("Angels", "number=?", new String[]{number});
            if (result == -1) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public Cursor getAngels(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Angels", null);
        return cursor;
    }

    public Cursor getAngels(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from Angels where name=?", new String[]{name});
        return cursor;
    }
}
